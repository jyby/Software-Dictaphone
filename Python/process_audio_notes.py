#!/usr/bin/python3
print("# Python Script to Process audionotes.")
print("# by Jeremy 'Le JyBy' Barbay")

location_of_audionotes_on_computer  = "/home/jbarbay/Unison/Boxes/MyBoxes/AudioNotesToProcess/"
location_of_archived_audionotes_on_computer  = "/home/jbarbay/References/AudioNotesArchived/"
location_of_archived_thoughts_on_computer  = "/home/jbarbay/References/Perso/Thoughts/AudioThoughts/"
debugLevel = 0  # 0=silent, 1=print and run all system calls, 2=only print system calls.
if debugLevel==1: print("\nDebug level 1: print and run all system calls")
if debugLevel==2: print("\nDebug level 2: only print system calls, do not execute them")
log_file = "nb_audionotes_log.txt" 

import os,shutil
import datetime
import logging # From Method 4 in https://www.askpython.com/python/built-in-methods/python-print-to-file
logging.basicConfig(filename='process_audionotes_log.txt', level=logging.DEBUG, format='')
import time
from pygame import mixer
print()

user_wants_to_continue = True
while user_wants_to_continue:
    
   if debugLevel>0: print("\n# Find most recent audionote folder, if any:")
   audionotes_folders =  [f for f in os.listdir(location_of_audionotes_on_computer) if os.path.isdir(os.path.join(location_of_audionotes_on_computer,f)) ]
   audionotes_folders.sort()
   if len(audionotes_folders) == 0:
       print("No audionotes folders to process: terminating.")
       break
   print(str(len(audionotes_folders))+" audio notes folders detected.")
   if debugLevel>0: print(str(audionotes_folders))
   audionotes_folder_to_process = audionotes_folders[-1]
   if debugLevel>0: print("I will process the folder `"+str(audionotes_folder_to_process)+"'.")
   folder_path = os.path.join(location_of_audionotes_on_computer,audionotes_folder_to_process)

   if debugLevel>0: print("\n# Find oldest audionote in selected folder, if any:")
   audionotes =  [f for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path,f)) ]
   audionotes.sort()
   print(str(len(audionotes))+" audio notes detected in the most recent folder.")
   if debugLevel>0: print(str(audionotes))
   if len(audionotes) == 0:
       print("No audionotes to process: removing folder.")
       if debugLevel<2: shutil.rmtree(folder_path)
   else: 
     audionote = audionotes[0]
     if debugLevel>0: print("I will process the audionote `"+str(audionote)+"'.")
     audionote_path = os.path.join(folder_path,audionote)

     if debugLevel>0: print("\n# Play audionote '"+str(audionote)+"':")
     file_mp3 = audionote_path
     mixer.init()
     mixer.music.load(file_mp3)
     mixer.music.play()

     if debugLevel>0: print("\n# Process audionote:")
     action_key = input("What to do with the file? [r:repeat, a:archive, t:thoughts, m:move, d:delete, q:quit] ")
     if action_key == "a":
       if debugLevel>0: print("\n# Move audionote to selected destination:")
       audionotes_archive_folder = os.path.join(location_of_archived_audionotes_on_computer,audionotes_folder_to_process)
       if not os.path.isdir(audionotes_archive_folder):
          if debugLevel>0: print("\n# Create folder '"+audionotes_archive_folder+"'.")
          if debugLevel<2: os.makedirs(audionotes_archive_folder)
       if debugLevel>0: print("Will move `"+audionote+"' to `"+audionotes_archive_folder+"'")
       if debugLevel<2: shutil.move(audionote_path,audionotes_archive_folder)
     elif action_key == "t":
       if debugLevel>0: print("\n# Move audionote to audiothoughts folder:")
       audionotes_archive_folder = os.path.join(location_of_archived_thoughts_on_computer,audionotes_folder_to_process)
       if not os.path.isdir(audionotes_archive_folder):
          if debugLevel>0: print("\n# Create folder '"+audionotes_archive_folder+"'.")
          if debugLevel<2: os.makedirs(audionotes_archive_folder)
       if debugLevel>0: print("Will move `"+audionote+"' to `"+audionotes_archive_folder+"'")
       if debugLevel<2: shutil.move(audionote_path,audionotes_archive_folder)
     elif action_key == 'm':
       print("m: Moving and Renaming the audio file")
       destination = input("Where should the file be moved? ")
       if debugLevel<2: shutil.move(audionote_path,destination)
     elif action_key == 'd':
       print("d: Deleting the audio file")
       if debugLevel<2: os.remove(audionote_path)
     elif action_key == 'r':
       print("r: Repeating the audio file")
     elif action_key == "q":
       print("q: Quitting the processin of Audio Notes")
       user_wants_to_continue = False
     else:
       print("Key '"+action_key+"' was entered but is not supported.")
   
   # if debugLevel>0: print("\n# Log number of audionotes processed:")
   # log_location = location_of_audionotes_on_computer+"/"+log_file
   # if not os.path.exists(log_location):
   #    with open(log_location, 'a') as f:
   #      if debugLevel<2: f.write('# Log produced by the scripts =processDictaphone.py= and =process_audio_notes.py=')
   #      if debugLevel<2: f.write('# %Y-%m-%d-%H:%M:%S\tcount\t ADDED/PROCESSED')
   # if debugLevel>0: print("Will log that "+str(len(audionotes))+" audionotes were added to '"+location_of_audionotes_on_computer+"'.")
   # with open(log_location, 'a') as f:
   #   if debugLevel<2: f.write(date.strftime("%Y-%m-%d-%H:%M:%S")+"\t"+"-"+str(len(audionotes))+"\t PROCESSED\n")

print("\nThat's all folks!\n")

