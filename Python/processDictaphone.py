#!/usr/bin/python3
print("# Python Script to Back-up audionotes from a Sony USB dictaphone.")
print("# by Jeremy 'Le JyBy' Barbay")
print()

location_of_audionotes = "/media/jbarbay/IC RECORDER/REC_FILE/AudioNotes/"
location_of_audionotes_on_computer  = "/home/jbarbay/Unison/Boxes/MyBoxes/AudioNotesToProcess/"
debugLevel = 0  # 0=silent, 1=print and run all system calls, 2=only print system calls.
if debugLevel==1: print("\nDebug level 1: print and run all system calls")
if debugLevel==2: print("\nDebug level 2: only print system calls, do not execute them")
log_file = "nb_audionotes_log.txt" 

import os,shutil
import datetime
# import logging # From Method 4 in https://www.askpython.com/python/built-in-methods/python-print-to-file
# logging.basicConfig(filename='process_dictaphone_log.txt', level=logging.DEBUG, format='')

if debugLevel>0: print("\n# Find first Dictaphone connected, if any:")
if not os.path.exists(location_of_audionotes):
  if debugLevel>0: print("No Dictaphone or Audionotes found")
  exit
else:
  if debugLevel>0: print("I will process the AudioNotes at `"+location_of_audionotes+"'.")

if debugLevel>0: print("\n# Create folder with today's date and time:")
date = datetime.datetime.now()
if debugLevel>0: print("Time is `"+str(date)+"'.")
date_suffix = date.strftime("%Y-%m-%d-%H%M")
if debugLevel>0: print("File Suffix is `"+str(date_suffix)+"'.")
destination_base = location_of_audionotes_on_computer+date_suffix
destination = destination_base
next_intent = 2
while os.path.exists(destination):
  destination = destination_base+"_"+str(next_intent)
  next_intent += 1
  if next_intent > 100:
     print("ERROR: more than 100 intents to create the destination folder.")
     exit
if debugLevel>0: print("I will create the folder `"+destination+"'.")
if debugLevel<2: os.makedirs(destination)

if debugLevel>0: print("\n# Move all audio notes to destination:")
audionotes = [f for f in os.listdir(location_of_audionotes)]
if debugLevel>0: print(str(len(audionotes))+" audionotes found")
for audionote in audionotes:
   if debugLevel>0: print("Will move '"+str(audionote)+"' to '"+destination+"/"+audionote+"'.")
   if debugLevel<2: shutil.move(location_of_audionotes+"/"+audionote,destination+"/"+audionote)
print(str(len(audionotes))+" audionotes found and moved")

if debugLevel>0: print("\n# Log number of audionotes added to the folder:")
log_location = location_of_audionotes_on_computer+"/"+log_file
if not os.path.exists(log_location):
   with open(log_location, 'a') as f:
     if debugLevel<2: f.write('# Log produced by the scripts =processDictaphone.py= and =processAudioNotes.py=')
     if debugLevel<2: f.write('# %Y-%m-%d-%H:%M:%S\tcount\t ADDED/PROCESSED')
if debugLevel>0: print("Will log that "+str(len(audionotes))+" audionotes were added to '"+location_of_audionotes_on_computer+"'.")
with open(log_location, 'a') as f:
  if debugLevel<2: f.write(date.strftime("%Y-%m-%d-%H:%M:%S")+"\t"+"+"+str(len(audionotes))+"\t ADDED\n")

print("\nThat's all folks!")

